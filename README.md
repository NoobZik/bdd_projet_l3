# Rapport de projet bdd_projet_l3 #

| Nom / Prénom   | Numéro Etudiant |
| :------------- | :-------------- |
| SHEIKH Rakib   | 11502605        |
| BOUAFIA Mehdi  | 11607146        |


## II - Conception de la base de donnée ##

1.  Proposition d'un modèle entité/association


![Image of Yaktocat](Sujet/ProjetBDD.png)

2.  Représentation de ce problème en modèle relationnel

*   Vu que sur markdown, on ne peut pas souligner, les clées primaires seront nommés de cette façon suivante : \_\_\_jesuiscleeprimaire\_\_\_
*   Les clées étrangères seront représenté par un symbole \*

```bash
Serie (___SerieID___, Titre, Annee, Pays, DateCreation)
Genre (___SerieID___*,___genre___)
Fondateur (___SerieID___*, ___Nom___, ___Prenom___, ___Role___)
Episode (
  ___EpisodeID___
  SerieID*,
  Saison,
  Titre_Episode,
  Duree,
  Datediffusion,
  Resume
)

Membre (___EpisodeID___*, ___Nom___, ___Prenom___,  ___Role___ )
Users (___Pseudo___, Sexe, DateSignUp, Age)

Avis (
  ___SerieID___*,
  ___EpisodeID___*,
  ___Pseudo___*,
  Commentaire,
  DateComment, 
  Note
)

ForumMessage (
  ___ForumMessageID___,
  Pseudo*,
  Message,
  DateMessage,
  MessageRepondu*
)
  
ForumSujet (
  ForumMessageID*
  Sujet,
  SerieID*
)

```

## III - Création ##

La création des tables qui sont conforme au problème et du modèle relationnel proposés sont accessible dans le fichier *creation.sql* comme demandé sur le sujet. Celle-ci possède aussi tout les contraintes que nous jugeons pertinentes.

Explications de nos choix :

Une série est composé d'un identifiant représentant une clée primaire, un titre, année, pays, datecreation.
Afin qu'une série soit valide pour l'insertion, il faut que les attributs tel que : titre, annee, datecreation et pays soient non null. En effets, ces attribut sont vitales pour définir précisément une série.

Une série peut avoir plusieurs types de genre. De ce fait, nous avons donc une association entre un genre et une série.

Une série peut être crée par une personne ayant pour rôle soit créateur, ou soit producteur. Nous avons donc une table Fondateur qui permet de répondre à ce besoin.


Un épisode est représenté par un identifiant unique, une clée étrangère faisant référence à une série, une saison, son titre, sa durée, date de diffusion et ainsi d'un résumé. Pour qu'une série soit valide, il faut que les saisont, titre, durées soit non nulle. Pour la durée il faut respecter une certaine mise en forme.

Pour représenter la listes des acteurs participant au tournage des séries, nous avons une classe membre qui est composé d'une clée étrangère vers un EpisodeID, un nom, prénom et son rôle. Cette classe membre peut etre utilisé pour la liste des acteurs et également pour la liste des réalisateurs.

Afin de représenter les utilisateurs du site, celle-ci est définit par un pseudo, sexe, date d'inscription et de sont age. Le pseudo est une clée primaire et il faut que les attributs age et date d'inscription soit non nulle. De plus le sexe accepte seulement les caractère "H" ou "F".

Un utilisateur peut rédiger un avis. Un avis est représenté par un commentaire, Date de commentaire et une note. Il y a aussi des attributs qui font référence à des clées étrangères tel que l'identifiant de la série, l'épisode, un pseudo.

Un utilisateur peut intéragir avec d'autre utilisateur par le bias du forum. Il y a deux tables pour les forum, représentant respectivement un sujet et les messages associés.
Le forum message est composé d'un identifiant unique qui représente aussi une clée primaire, un message, une date du message. Elle possède une référence reflexive qui est le message répondu.
Pour le forum sujet, celle-ci est aussi représenté par un identifiant unique faisant référence à une clée primaire et contient une référence étrangère vers un identifiant de série.




## IV - Insertion ##

L'insertions des données dans la table se fait avec le fichier *insertion.sql*. Ces données reflètent au maximum les informations réel ou bien fictives que nous avons pu récolter.

## V - Interrogation ##

1.  Quel est la liste des séries de la base ?

```sql
select * from serie;
```

```bash
SERIEID  TITRE              ANNEE   PAYS        DATECREATION
1	Supernatural            2005    Etats-Unis  09/13/2005
2	The Big Bang Theory     2007    Etats-Unis  09/24/2007
3	Shingeki no Kyojin      2013    Japon       04/07/2013
4	One Piece               1999    Japon       10/20/1999
5	Rick et Morty           2013    Etats-Unis  12/02/2013
6	South Park              1997    Etats-Unis  08/13/1997
7	Doctor Who              1963    Royaume-Uni 11/23/1963
8	Doctor Who              2005    Royaume-Uni 04/26/2005
9	Mobile Suit Gundam      1979    Japon       01/26/1979
10	Mobile Suit Zeta Gundam 1985    Japon       04/02/1985
11	Mobile Suit Gundam ZZ   1986    Japon       07/01/1986
12	La casa de papel        2017    Espagne     05/02/2017
13	Vikings                 2013    Canada      04/03/2013
14	Minus et Cortex         1995    Etats-Unis  09/09/1995
15	Breaking Bad            2008    Etats-Unis  01/20/2008
16	Malcolm                 2000    Etats-Unis  01/09/2000
```

2.  Combien de pays différents ont créé des séries dans notre base ?

```sql
select count(distinct pays) from serie;
```

```bash
COUNT(DISTINCTPAYS)
5
```
3.  Quels sont les titres des séries originaires du Japon, triés par titre ?

```sql
select Titre from serie where pays = 'Japon' order by Titre;
```

```bash
TITRE
Mobile Suit Gundam
Mobile Suit Gundam ZZ
Mobile Suit Zeta Gundam
One Piece
Shingeki no Kyojin
```
4.  Combien y a-t-il de séries originaires de chaque pays ?

```sql
select pays, count(*) from serie group by pays;
```

```bash
PAYS	    COUNT(*)
Etats-Unis	7
Royaume-Uni	2
Canada	    1
Japon	    5
Espagne	    1
```
5.  Combien de séries ont été créés entre 2001 et 2015?

```sql
select count(*) from serie where Annee between 2001 and 2015;
```

```bash
COUNT(*)
7
```
6.  Quelles séries sont à la fois du genre « Comédie » et « Science-Fiction » ?

```sql
select Titre,Annee from serie where SerieID in (select SerieID from genre where genre = 'Comédie') Intersect select Titre,Annee from serie where SerieID in (select SerieID from genre where genre =  'Science-Fiction');
```

```bash
TITRE	        ANNEE
Rick et Morty	2013
```
7.  Quels sont les séries produites par « Spielberg », affichés par date décroissantes ?

```sql
select Titre,Annee from serie where SerieID in (select SerieID from fondateur where nom = 'Spielberg' and role = 'Producteur') order by datecreation desc;
```

```bash
TITRE	        ANNEE
Minus et Cortex	1995
```
8.  Afficher les séries Américaines par ordre de nombre de saisons croissant.

```sql
select distinct t1.SerieID, count(distinct t1.saison) from episode t1 where t1.SerieID in (select t2.SerieID from serie t2 where t2.pays = 'Etats-Unis') group by t1.SerieID order by count(distinct t1.saison) desc;
```

```bash
SERIEID	COUNT(DISTINCTT1.SAISON)
2	    4
5	    2
1	    2
6	    2
16	    1
14	    1
15	    1
```
9.  Quelle série a le plus d’épisodes ?

```sql
select SerieID from episode having count(*) = (select max(count(*)) from episode group by SerieID) group by SerieID;
```

```bash
SERIEID
5
```
10. La série « Big Bang Theory » est-elle plus appréciée des hommes ou des femmes ?

```sql
select avg(t1.note) "Homme",avg(t2.note) "Femme" from avis t1, avis t2 where t1.SerieID = 2 and t2.SerieID = 2 and t1.pseudo in(select pseudo from users where sexe = 'M') and t2.pseudo in(select pseudo from users where sexe = 'F');

```

```bash
Homme	Femme
8	    9
```
11. Affichez les séries qui ont une note moyenne inférieure à 5, classé par note.

```sql
select SerieID, avg(note) from avis where EpisodeID is NULL group by SerieID Having avg(note) < 5;
```

```bash
SERIEID	AVG(NOTE)
7	    2
```
12. Pour chaque série, afficher le commentaire correspondant à la meilleure note.

```sql
select distinct t1.SerieID, t1.commentaire from avis t1 where t1.note = (select max(t2.note) from avis t2 where t1.serieID = t2.serieID group by t2.SerieID); 
```

```bash
SERIEID	COMMENTAIRE
6	    pas mal
1	    WOAW !!!
3	    -
7	    j\ai rien compris
9	    pas mal
2	    TRO BI1
8	    pas mal
11	    pas mal
5	    pas mal
2	    j\aime bien
12	    pas mal
15	    pas mal
16	    excellent my froueennd
10	    pas mal
13	    pas mal
14	    pas mal
```
13. Affichez les séries qui ont une note moyenne sur leurs épisodes supérieure à 8.

```sql
select SerieID from avis where EpisodeID is NOT NULL group by SerieID  having avg(note)>8;
```

```bash
SERIEID
16
```
14. Afficher le nombre moyen d’épisodes des séries avec l’acteur « Bryan Cranston ».

```sql
select avg(count(EpisodeID)) from episode where EpisodeID in (select EpisodeID from membre where role = 'Acteur' and nom = 'Cranston' and prenom = 'Bryan') group by SerieID;
```

```bash
AVG(COUNT(EPISODEID))
2
```
15. Quels acteurs ont réalisé des épisodes de série ?

```sql
select nom, prenom from membre where role = 'Acteur' Intersect select nom, prenom from membre where role = 'Realisateur';
```

```bash
aucune donnée n'a été trouvée
```
16. Quels acteurs ont joué ensemble dans plus de 80% des épisodes d’une série ?

```sql
select t1.Nom,t1.prenom, t2.nom,t2.prenom from membre t1,membre t2 where t1.role = acteur and t2.role = acteur having count(EpisodeID)/(select count(*) from episode group by SerieID) > 80/100 group by SerieID
```

```bash

```
17. Quels acteurs ont joué dans tous les épisodes de la série « Breaking Bad » ?

```sql
select nom, prenom from membre where EpisodeID  in (select EpisodeID from episode where serieID = (select serieID from serie where titre = 'Breaking Bad')) having count(*) = (select count(*) from episode where SerieID  = (select SerieID from serie where titre = 'Breaking Bad')) group by nom, prenom;
```

```bash
NOM	                PRENOM
Per Sullivan	    Erik
Berfield	        Justin
Kennedy Masterson	Christopher
Lloyd Burns	        Catherine
Kaczmarek	        Jane
Cranston	        Bryan
Muniz	            Frankie
```
18. Quels utilisateurs ont donné une note à chaque série de la base ?

```sql
select t1.pseudo from users t1 where not exists(select * from serie t2 where not exists (select * from avis t3 where t1.pseudo = t3.pseudo  and t3.SerieID = t2.SerieID and note is not NULL));
```

```bash
aucune donnée n'a été trouvée
```
19. Pour chaque message, affichez son niveau et si possible le titre de la série en question.

```sql
select Message,Level from ForumMessage start with MessageReponse = Null CONNECT BY MessageReponse = PRIOR ForumMessageID;
```

```bash
MESSAGE	LEVEL
c\est quoi la serie avec bébé Yoda ?	                    1
C THE MANDALORIAN	                                        2
Ok merci Kikoo	                                            3
Malcolm est bien comme serie ?	                            1
ouais c\est pas mal	                                        2
c\est moi ou tu sais que dire c\est pas mal Julia12 ? XD	3
PTDRRRRRRRRR C TRO VRÉ MDRRRRRRRRRRRRRR LOOOOOOOL XD	    4
c\est plus que pas mal Malcolm c\est super drole !	        3
Yes. no. maybe. i don\t know. can you reapeat the question?	2
```
20. Les messages initiés par « Azrod95 » génèrent combien de réponses en moyenne ?

```sql
select avg(count(*)) from ForumMessage where MessageRepondu in (select ForumMessageID from ForumMessage where pseudo = 'Azrod95') group by message;
```

```bash
AVG(COUNT(*))
-------------
            1
```