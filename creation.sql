/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   creation.sql                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: noobzik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/22 15:14:39 by noobzik           #+#    #+#             */
/*   Updated: 2019/11/22 15:14:39 by noobzik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
DROP TABLE serie CASCADE CONSTRAINTS;
DROP TABLE genre CASCADE CONSTRAINTS;
DROP TABLE Fondateur CASCADE CONSTRAINTS;
DROP TABLE Episode CASCADE CONSTRAINTS;
DROP TABLE Membre CASCADE CONSTRAINTS;
DROP TABLE Users CASCADE CONSTRAINTS;
DROP TABLE Avis CASCADE CONSTRAINTS;
DROP TABLE ForumMessage CASCADE CONSTRAINTS;
DROP TABLE ForumSujet CASCADE CONSTRAINTS;



CREATE TABLE  serie (
  SerieID          NUMBER(4),
  Titre            VARCHAR2(25),
  Annee            NUMBER(4),
  Pays             VARCHAR2(25),
  Datecreation     Date,
  CONSTRAINT PK_SERIE PRIMARY KEY (SerieID),
  CONSTRAINT NN_SERIE_TITRE CHECK (Titre IS NOT NULL),
  CONSTRAINT NN_SERIE_ANNEE CHECK (Annee IS NOT NULL),
  CONSTRAINT NN_SERIE_DATECREATION CHECK (Datecreation IS NOT NULL),
  CONSTRAINT NN_SERIE_PAYS CHECK (Pays IS NOT NULL)
);



CREATE TABLE  genre (
  SerieID          NUMBER(4),
  genre            VARCHAR2(25),
  CONSTRAINT PK_GENRE PRIMARY KEY (SerieID,genre),
  CONSTRAINT FK_GENRE_SERIE FOREIGN KEY (SerieID) REFERENCES serie (SerieID),
  CONSTRAINT NN_GENRE_genre CHECK (genre IS NOT NULL)
);



CREATE TABLE  Fondateur (
  SerieID          NUMBER(4),
  Nom              VARCHAR2(25),
  Prenom           VARCHAR2(25),
  Role             VARCHAR2(25),
  CONSTRAINT PK_FONDATEUR PRIMARY KEY (SerieID,Nom,Prenom,Role),
  CONSTRAINT FK_FONDATEUR_SERIE FOREIGN KEY (SerieID) REFERENCES serie (SerieID),
  CONSTRAINT NN_FONDATEUR_NOM CHECK (NOM IS NOT NULL),
  CONSTRAINT NN_FONDATEUR_PRENOM CHECK (PRENOM IS NOT NULL),
  CONSTRAINT CK_FONDATEUR_ROLE CHECK (ROLE IN ('Createur','Producteur'))
);



CREATE TABLE  Episode (
  EpisodeID        NUMBER(4),
  SerieID          NUMBER(4),
  Saison           NUMBER(2),
  Titre_Episode    VARCHAR2(70),
  Duree            VARCHAR2(8),
  Datediffusion    DATE,
  Resume           VARCHAR2(1700),
  CONSTRAINT PK_EPISODE_EPISODEID PRIMARY KEY (EpisodeID),
  CONSTRAINT FK_EPISODE_SERIE FOREIGN KEY (SerieID) REFERENCES serie (SerieID),
  CONSTRAINT NN_EPISODE_SAISON CHECK (SAISON IS NOT NULL),
  CONSTRAINT NN_EPISODE_TITRE_EPISODE CHECK (TITRE_EPISODE IS NOT NULL),
  CONSTRAINT NN_EPISODE_DUREE CHECK (DUREE IS NOT NULL),
  constraint ck_duree_1 check(substr(duree,3,1) = ':'),
  constraint ck_duree_2	check(substr(duree,6,1) = ':'),
  constraint ck_duree_sec check(substr(duree,7,2) between 0 and 59),
  constraint ck_duree_min check(substr(duree,4,2) between 0 and 59),
  constraint ck_duree_h check(substr(duree,1,2) between 0 and 99)
);



CREATE TABLE  Membre (
  EpisodeID        NUMBER(4),
  Nom              VARCHAR2(25),
  Prenom           VARCHAR2(25),
  Role             VARCHAR2(25),
  CONSTRAINT PK_MEMBRE PRIMARY KEY (EpisodeID,Nom,Prenom,Role),
  CONSTRAINT FK_MEMBRE_EPISODE FOREIGN KEY (EpisodeID) REFERENCES EPISODE (EpisodeID),
  CONSTRAINT NN_MEMBRE_NOM CHECK (NOM IS NOT NULL),
  CONSTRAINT NN_MEMBRE_PRENOM CHECK (PRENOM IS NOT NULL),
  CONSTRAINT CK_MEMBRE_ROLE CHECK (ROLE IN ('Realisateur','Acteur'))
);



CREATE TABLE  Users (
  Pseudo          VARCHAR2(25),
  Sexe            CHAR(1),
  DateSignUp      DATE,
  Age             NUMBER(3),
  CONSTRAINT PK_USERS PRIMARY KEY (Pseudo),
  CONSTRAINT NN_USERS_AGE CHECK (Age IS NOT NULL),
  CONSTRAINT CK_USERS_SEXE CHECK (Sexe IN ('F', 'M')),
  CONSTRAINT NN_USERS_DATESIGNUP CHECK (DateSignUp IS NOT NULL)
);



CREATE TABLE  Avis (
  SerieID         NUMBER(4),
  EpisodeID       NUMBER(4),
  Pseudo          VARCHAR2(25),
  Commentaire     VARCHAR2(50),
  DateComment     DATE,
  Note            NUMBER(2),
  CONSTRAINT UQ_AVIS UNIQUE (SerieID,EpisodeID,Pseudo),
  CONSTRAINT FK_AVIS_USERS FOREIGN KEY (Pseudo) REFERENCES USERS (Pseudo),
  CONSTRAINT FK_AVIS_EPISODE FOREIGN KEY (EpisodeID) REFERENCES EPISODE (EpisodeID),
  CONSTRAINT FK_AVIS_SERIE FOREIGN KEY (SerieID) REFERENCES SERIE (SerieID),
  CONSTRAINT CK_AVIS_NOTE CHECK (Note BETWEEN 0 AND 10),
  CONSTRAINT NN_AVIS_DATECOMMENT CHECK (DateComment IS NOT NULL)
);



CREATE TABLE  ForumMessage (
  ForumMessageID  NUMBER(4),
  Pseudo          VARCHAR2(25),
  Message         VARCHAR2(100),
  DateMessage     DATE,
  MessageRepondu  NUMBER(4),
  CONSTRAINT PK_FORUMMESSAGE PRIMARY KEY (ForumMessageID),
  CONSTRAINT FK_FORUMMESSAGE_USERS FOREIGN KEY (Pseudo) REFERENCES USERS (Pseudo),
  CONSTRAINT FK_FORUMMESSAGE_FORUMMESSAGE FOREIGN KEY (MessageRepondu) REFERENCES FORUMMESSAGE (ForumMessageID),
  CONSTRAINT NN_FORUMMESSAGE_MESSAGE CHECK (Message IS NOT NULL),
  CONSTRAINT NN_FORUMMESSAGE_DATEMESSAGE CHECK (DateMessage IS NOT NULL)
);



CREATE TABLE  ForumSujet (
  ForumMessageID  NUMBER(4),
  Sujet           VARCHAR2(25),
  SerieID         NUMBER(4),
  CONSTRAINT FK_FORUMMESSAGE_FORUMESSAGEID FOREIGN KEY (ForumMessageID) REFERENCES FORUMMESSAGE (ForumMessageID),
  CONSTRAINT FK_FORUMMESSAGE_SERIEID FOREIGN KEY (SerieID) REFERENCES SERIE (SerieID)
);
