insert into serie (SerieID,titre, annee, pays, Datecreation) values(1,'Supernatural',2005,'Etats-Unis','13/09/2005');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(2,'The Big Bang Theory',2007,'Etats-Unis','24/09/2007');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(3,'Shingeki no Kyojin',2013,'Japon','07/04/2013');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(4,'One Piece',1999,'Japon','20/10/1999');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(5,'Rick et Morty',2013,'Etats-Unis','02/12/2013');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(6,'South Park',1997,'Etats-Unis','13/08/1997');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(7,'Doctor Who',1963,'Royaume-Uni','23/11/1963');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(8,'Doctor Who',2005,'Royaume-Uni','26/04/2005');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(9,'Mobile Suit Gundam',1979,'Japon','26/01/1979');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(10,'Mobile Suit Zeta Gundam',1985,'Japon','02/04/1985');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(11,'Mobile Suit Gundam ZZ',1986,'Japon','01/07/1986');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(12,'La casa de papel',2017,'Espagne','02/05/2017');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(13,'Vikings',2013,'Canada','03/04/2013');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(14,'Minus et Cortex',1995,'Etats-Unis','09/09/1995');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(15,'Breaking Bad',2008,'Etats-Unis','20/01/2008');
insert into serie (SerieID,titre, annee, pays, Datecreation)values(16,'Malcolm',2000,'Etats-Unis','09/01/2000');




insert into genre (SerieID, genre) values (1,'Fantastique');
insert into genre (SerieID, genre) values (1,'Horreur');
insert into genre (SerieID, genre) values (1,'Thriller');
insert into genre (SerieID, genre) values (1,'Drame');
insert into genre (SerieID, genre) values (2,'Comédie');
insert into genre (SerieID, genre) values (3,'Action');
insert into genre (SerieID, genre) values (3,'Horreur');
insert into genre (SerieID, genre) values (3,'Thriller');
insert into genre (SerieID, genre) values (3,'Drame');
insert into genre (SerieID, genre) values (3,'Animation');
insert into genre (SerieID, genre) values (4,'Action');
insert into genre (SerieID, genre) values (4,'Comédie');
insert into genre (SerieID, genre) values (4,'Aventure,');
insert into genre (SerieID, genre) values (4,'Fantastique');
insert into genre (SerieID, genre) values (4,'Animation');
insert into genre (SerieID, genre) values (5,'Fantastique');
insert into genre (SerieID, genre) values (5,'Science-Fiction');
insert into genre (SerieID, genre) values (5,'Comédie');
insert into genre (SerieID, genre) values (5,'Animation');
insert into genre (SerieID, genre) values (6,'Comédie');
insert into genre (SerieID, genre) values (6,'Animation');
insert into genre (SerieID, genre) values (7,'Science-Fiction');
insert into genre (SerieID, genre) values (8,'Science-Fiction');
insert into genre (SerieID, genre) values (9,'Science-Fiction');
insert into genre (SerieID, genre) values (9,'Animation');
insert into genre (SerieID, genre) values (10,'Drame');
insert into genre (SerieID, genre) values (10,'Animation');
insert into genre (SerieID, genre) values (10,'Science-Fiction');
insert into genre (SerieID, genre) values (11,'Animation');
insert into genre (SerieID, genre) values (11,'Science-Fiction');
insert into genre (SerieID, genre) values (12,'Thriller');
insert into genre (SerieID, genre) values (12,'Drame');
insert into genre (SerieID, genre) values (13,'Drame');
insert into genre (SerieID, genre) values (14,'Animation');
insert into genre (SerieID, genre) values (14,'Comédie');
insert into genre (SerieID, genre) values (15,'Thriller');
insert into genre (SerieID, genre) values (15,'Drame');
insert into genre (SerieID, genre) values (16,'Comédie');




insert into fondateur(SerieID,prenom,nom,role) values(1,'Eric','Kripke','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(1,'Joseph','McGinty Nichol','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(1,'Eric','Kripke','Createur');

insert into fondateur(SerieID,prenom,nom,role) values(2,'Chuck','Lorre','Createur');
insert into fondateur(SerieID,prenom,nom,role) values(2,'Bill','Prady','Createur');
insert into fondateur(SerieID,prenom,nom,role) values(2,'Chuck','Lorre','Producteur');

insert into fondateur(SerieID,prenom,nom,role) values(3,'Tetsuya','Kinoshita','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(3,'George','Wada','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(3,'Hajime','Isayama','Createur');

insert into fondateur(SerieID,prenom,nom,role) values(4,'Oda','	Eiichiro','Createur');

insert into fondateur(SerieID,prenom,nom,role) values(5,'Justin','Roiland','Createur');
insert into fondateur(SerieID,prenom,nom,role) values(5,'Dan','Harmon','Createur');
insert into fondateur(SerieID,prenom,nom,role) values(5,'Justin','Roiland','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(5,'Pete','Michels','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(5,'Wes','Archer','Producteur');

insert into fondateur(SerieID,prenom,nom,role) values(6,'Trey','Parker','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(6,'Matt','Stone','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(6,'Anne','Garefino','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(6,'Trey','Parker','Createur');
insert into fondateur(SerieID,prenom,nom,role) values(6,'Matt','Stone','Createur');

insert into fondateur(SerieID,prenom,nom,role) values(7,'Sydney','Newman','Createur');
insert into fondateur(SerieID,prenom,nom,role) values(7,'Donald','Wilson','Createur');
insert into fondateur(SerieID,prenom,nom,role) values(7,'Verity','Lambert','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(7,'John','Nathan-Turner','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(7,'John','Wiles','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(7,'Barry','Letts','Producteur');

insert into fondateur(SerieID,prenom,nom,role) values(8,'Stephen Russell','Davies','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(8,'Phil','Collinson','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(8,'Julie','Gardner','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(8,'Steven','Moffat','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(8,'Chris','Chibnall','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(8,'Stephen Russell','Davies','Createur');

insert into fondateur(SerieID,prenom,nom,role) values(9,'Yu','Okazaki','Createur');
insert into fondateur(SerieID,prenom,nom,role) values(9,'Yoshiyuki','Tomino','Producteur');

insert into fondateur(SerieID,prenom,nom,role) values(10,'Yoshiyuki','Tomino','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(10,'Kazuhisa','Kondo','Createur');

insert into fondateur(SerieID,prenom,nom,role) values(11,'Yoshiyuki','Tomino','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(11,'Toshiya','Murakami','Createur');

insert into fondateur(SerieID,prenom,nom,role) values(12,'Álex','Pina','Createur');
insert into fondateur(SerieID,prenom,nom,role) values(12,'Jesús','Colmenar','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(12,'Sonia','Martínez','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(12,'Álex','Pina','Producteur');

insert into fondateur(SerieID,prenom,nom,role) values(13,'Michael','Hirst','Createur');
insert into fondateur(SerieID,prenom,nom,role) values(13,'Keith','Thompson','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(13,'Steve','Wakefield','Producteur');

insert into fondateur(SerieID,prenom,nom,role) values(14,'Steven','Spielberg','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(14,'Tom','Ruegger','Createur');

insert into fondateur(SerieID,prenom,nom,role) values(15,'Vince','Gilligan','Createur');
insert into fondateur(SerieID,prenom,nom,role) values(15,'Mark','Johnson','Producteur');

insert into fondateur(SerieID,prenom,nom,role) values(16,'Maggie','Bandur','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(16,'Dan','Kopelman','Producteur');
insert into fondateur(SerieID,prenom,nom,role) values(16,'Linwood','Boomer','Createur');




insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (1,1,1,'La Dame blanche','00:42:00','13/09/2005','Le soir du 2 novembre 1983 dans la maison des Winchester, Dean et Sam sont encore des enfants lorsque leur mère se fait tuer par une force invisible et démoniaque. Avec leur père, ils arrivent à fuir du terrible incendie qui ravage leur maison. 22 ans plus tard, Sam (le cadet) vit avec sa petite amie Jessica, envisage déjà une vie toute tracée. Mais voilà que son frère Dean sintroduit une nuit chez lui pour lui faire part de quelque chose qui linquiète ; leur père a disparu depuis plusieurs jours alors quil était en train de "chasser". Se pliant finalement à la demande de son frère, Sam accepte de laccompagner. Les indices vont les mener dans une petite ville où des hommes disparaissent lors daccidents de voiture. Daprès eux, une dame blanche serait responsable de cela.');
insert into membre(EpisodeID, prenom, nom, role) values(1, 'Jared','Padalecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(1, 'Jensen','Ackles', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(1, 'Jeffrey','Dean Morgan', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(1, 'Sarah','Shahi', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(1, 'Adrianne','Palicki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(1, 'Ross','Kohn', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(1, 'Steve','Railsback', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (2,1,1,'Wendigo','00:42:00','20/09/2005','Alors que les frères viennent de régler leur première enquête ensemble, Sam peine à réaliser que sa petite amie Jessica a été tuée par le même démon qui a assassiné sa mère. Les prochains indices laissés par leur père les emmènent jusquà une jeune fille qui ne retrouve pas son frère parti camper en forêt. Aidés par un garde forestier, les deux frères se rendent compte, une fois au campement complètement détruit, que la cause de sa disparition nest pas liée à un animal mais à un Wendigo, une créature cannibale terriblement coriace.');
insert into membre(EpisodeID, prenom, nom, role) values(2, 'Jared','Padalecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(2, 'Jensen','Ackles', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(2, 'Alden','Ehrenreich', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(2, 'Callum','Keith Rennie', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(2, 'Gina','Holden', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(2, 'Graham','Wardle', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values(3,1,1,'L\Esprit du lac','00:42:00','27/09/2005','Les frères Winchester sont perplexes face à une noyade qui a eu lieu dans une petite bourgade. Là-bas, ils rencontrent une jeune femme et son fils muet. Apparemment, ce dernier aurait été témoin de quelque chose près du lac. Après une nouvelle victime (cette fois noyée dans un lavabo) qui appartenait à la même famille que la précédente, les frères découvrent un terrible secret enfoui depuis des années ; un enfant a été tué et son esprit entreprend de se venger de ses meurtriers en détruisant leurs familles un par un.');
insert into membre(EpisodeID, prenom, nom, role) values(3, 'Jared','Padalecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(3, 'Jensen','Ackles', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(3, 'Amy','Acker', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(3, 'Nico','McEown', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(3, 'Amber','Borycki', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (4,1,1,'Le Fantôme voyageur','00:42:00','04/10/2005','Un ancien ami des Winchester appelle les deux frères pour leur demander d\enquêter sur un étrange crash aérien. D\après un témoignage, un homme au regard sombre aurait purement et simplement ouvert la porte de l\appareil et aurait conduit ce dernier à la catastrophe. Peu après, sur les sept survivants, le pilote précipite un petit avion de tourisme au sol et se tue. Pour les deux frères, il n\y a pas de doute ; un démon prend possession d\un passager pour crasher les appareils.');
insert into membre(EpisodeID, prenom, nom, role) values(4, 'Jared','Padalecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(4, 'Jensen','Ackles', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(4, 'Brian','Markinson', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(4, 'Jaime','Ray Newman', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(4, 'Kett','Turton', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (5,1,2,'Le Sacrifice','00:42:00','28/09/2006','Après avoir été percuté de plein fouet par un semi-remorque, Sam, Dean et leur père sont transférés à l\hôpital. Sam et son père s\en sortent relativement bien ce qui n\est pas le cas de Dean qui est dans le coma. Malgré tout, son esprit erre dans les couloirs de l\hôpital en cherchant à communiquer avec son frère. Il va y rencontrer un esprit qui pourrait bien se révéler être un faucheur qui se charge d\accompagner les âmes défuntes dans l\au-delà. Pendant ce temps, son père décide de tout faire pour qu\il revienne à lui et appelle le démon aux yeux jaunes.');
insert into membre(EpisodeID, prenom, nom, role) values(5, 'Jared','Padalecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(5, 'Jensen','Ackles', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(5, 'Jeffrey','Dean Morgan', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(5, 'Jim','Beaver', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(5, 'Fredric','Lehne', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(5, 'Lindsey','McKeon', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (6,1,2,'Le Clown','00:42:00','05/10/2006','À la suite du décès de leur père, Sam et Dean tentent de trouver des indices sur son passé. Ils trouvent un message téléphonique d\une certaine Ellen qui tient un bar (le Roadhouse). Après s\y être rendu, ils font la connaissance de ladite Ellen, de sa fille Jo et d\un génie au comportement étrange, Ash. Durant leur discussion, les frères Winchester tombent sur un article de journal évoquant des meurtres étranges impliquant un clown. Parvenant à retrouver la fête foraine d\où viendrait la créature, ils font la connaissance de Barry, un vieil aveugle lanceur de couteaux et de M. Cooper, le gérant de la fête. Parvenant à se faire engager, ils vont réussir à sauver une famille de la créature avant de découvrir que M. Cooper pourrait être derrière tout ça. Malheureusement, ils se trompent car Barry est le coupable et le seul moyen de tuer un Rakshasa, c\est avec du cuivre et Dean va demander à Barry s\il n\aurait pas un couteau en cette matière.');
insert into membre(EpisodeID, prenom, nom, role) values(6, 'Jared','Padalecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(6, 'Jensen','Ackles', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(6, 'Alona','Tal', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(6, 'Chad','Lindberg', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(6, 'Samantha','Ferris', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(6, 'Alec','Willows', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (7,1,2,'Au-delà des apparences','00:42:00','12/10/2006','L\Impala est enfin réparée. Sam et Dean s\aventure à Red Lodge pour enquêter sur des décapitations ainsi que du massacre de bétail. Sur place, ils découvrent que l\une des victimes était en fait un vampire. Lors d\une soirée dans un bar, ils vont faire la connaissance de Gordon, un chasseur de vampires. Ce dernier leur révèle qu\il traque un grand groupe de suceur de sang depuis maintenant plus d\une année et qu\il compte bien tous les tuer. Bien que les Winchester proposent leur aide, Gordon décline et s\en va seul. Dans la nuit, il attaque un vampire et se retrouve piégé par la créature. Les deux frères arrivent à temps pour le sauver. Ils vont fêter ça dans un bar des environs. Quelque chose dérange Sam et il s\en va seul au motel tandis que Dean et Gordon continuent de discuter. Sam se fait alors kidnapper par des vampires, notamment une fille, Lenore. Cette dernière lui apprend qu\ils ne font plus de mal aux humains et se nourrissent de sang de bétail. Ils laissent partir Sam qui tente de convaincre son frère qu\ils n\ont rien de dangereux et que la véritable menace vient en fait de Gordon.');
insert into membre(EpisodeID, prenom, nom, role) values(7, 'Jared','Padalecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(7, 'Jensen','Ackles', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(7, 'Sterling','K. Brown', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(7, 'Amber','Benson', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(7, 'Samantha','Ferris', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(7, 'Ty','Olsson', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (8,1,2,'Vengeance d\outre-tombe','00:42:00','19/10/2006','Alors que les frères Winchester se rendent sur la tombe de leur mère, Dean découvre qu\une autre tombe semble tuer tout ce qui se trouve autour d\elle (fleurs, herbe, etc.). La jeune fille qui y est enterrée, Angela Mason, est morte récemment. Ils vont voir le père de cette fille qui est complètement bouleversé par ce coup du sort et qui possède un étrange livre de grec ancien. C\est après la mort de l\ex petit ami d\Angela et le fait d\avoir subtilisé son journal intime que les frères découvrent l\existence d\un confident de la jeune fille. Ce dernier semble très nerveux mais plus bouleversé qu\autre chose. Ils apprennent qu\elle est morte dans un accident de la route après une dispute avec son petit ami. Ils en déduisent que quelqu\un l\a fait revenir d\entre les morts. Accusant tout d\abord le père de la jeune fille, ils vont se rendre compte de leur erreur car c\est son confident qui la garde chez lui. Après avoir tenté de tuer sa meilleure amie, elle va tout faire pour empêcher Sam et Dean de la détruire en la clouant dans son propre cercueil.');
insert into membre(EpisodeID, prenom, nom, role) values(8, 'Jensen','Ackles', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(8, 'Tamara','Feldman', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(8, 'Christopher','Jacot', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(8, 'Serge','Houde', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(8, 'Jared','Keeso', 'Acteur');



insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (9,2,1,'La Nouvelle Voisine des surdoués','00:22:00','24/09/2007','Leonard et Sheldon se rendent à la banque de sperme afin de faire un don au QI élevé, mais poussés par les remords, ils décident de rentrer. De retour dans leur immeuble, ils font la rencontre d\une jeune fille très attirante : Penny, leur nouvelle voisine de palier, originaire d\Omaha. En l\invitant à déjeuner, ils font rapidement connaissance et découvrent le grand côté extraverti et chaleureux de la jeune fille. Howard et Raj font également connaissance avec Penny.Sous le charme de Penny, Leonard accepte de lui rendre un petit service : se rendre chez son ex-petit ami afin de récupérer une télévision. Il s\avère que son ex-copain est exceptionnellement musclé, et malgré l\appui de Sheldon, tous deux se font racketter. Afin de se faire pardonner, Penny invite les quatre surdoués à sortir dîner.');
insert into membre(EpisodeID, prenom, nom, role) values(9, 'Johnny','Galecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(9, 'Jim','Parsons', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(9, 'Kunal','Nayyar', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(9, 'Simon','Helberg', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(9, 'Kaley','Cuoco', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(9, 'Vernee','Watson-Johnson', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(9, 'Brian','Patrick Wade', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (10,2,1,'Des voisins encombrants','00:22:00','1/10/2007','Penny interrompt les quatre amis en plein repas thaï alors qu\ils s\apprêtent à regarder l\intégrale des films Superman. Elle demande à Leonard de signer à sa place la réception d\un paquet, qui devrait être livré le lendemain pendant qu\elle travaille. Leonard accepte en espérant l\impressionner, et reçoit le double de la clé de l\appartement de Penny. Il s\avère que le colis est un meuble Ikea très encombrant, et avant que Leonard ne puisse demander au livreur de le monter, ce dernier est déjà parti. Grâce à des hypothèses mathématiques et à l\aide de Sheldon, il arrive finalement à transporter le paquet dans l\appartement de Penny.');
insert into membre(EpisodeID, prenom, nom, role) values(10, 'Johnny','Galecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(10, 'Jim','Parsons', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(10, 'Kunal','Nayyar', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(10, 'Simon','Helberg', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(10, 'Kaley','Cuoco', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (11,2,2,'Un secret bien gardé','00:22:00','22/09/2008','Ne sachant quelle attitude adopter dans sa nouvelle relation avec Leonard, Penny se tourne vers Sheldon et lui confie un de ses secrets : elle a dit à Leonard qu\elle avait réussi à la fac alors que c\est faux. Sheldon a tellement peur de révéler ce secret à Leonard qu\il lui annonce qu\il va déménager, sans lui en indiquer la raison. Il emménage d\abord chez Raj mais celui-ci, n\arrivant pas à dormir à cause de Sheldon, le confie à Wolowitz qui, pour être tranquille, lui donne du lait chaud avec une forte dose de Valium avant de le ramener chez Leonard. Là-bas, et sous l\effet du Valium, il révèle le secret à Leonard. Le lendemain, Leonard a une explication avec Penny à ce sujet et ils finissent par se disputer.');
insert into membre(EpisodeID, prenom, nom, role) values(11, 'Johnny','Galecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(11, 'Jim','Parsons', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(11, 'Kunal','Nayyar', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(11, 'Simon','Helberg', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(11, 'Kaley','Cuoco', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (12,2,2,'Le Flirt de Léonard','00:22:00','29/09/2008','Alors que les quatre garçons rentrent de la fête de la renaissance, ils croisent Penny dans l\escalier en compagnie de son nouveau petit ami, Eric. Leonard déclare alors qu\il est donc libre de sortir avec qui il veut. Lorsque Leslie apprend que l\histoire entre Leonard et Penny est terminée, elle tente de le séduire à nouveau. Lors d\un rendez-vous entre Leonard et Leslie, Sheldon se dispute avec Leslie au sujet de théories scientifiques. Leonard étant d\accord avec l\hypothèse de Sheldon, elle le quitte.');
insert into membre(EpisodeID, prenom, nom, role) values(12, 'Johnny','Galecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(12, 'Jim','Parsons', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(12, 'Kunal','Nayyar', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(12, 'Simon','Helberg', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(12, 'Kaley','Cuoco', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (13,2,3,'La Fluctuation de l\ouvre-boîte électrique','00:22:00','21/09/2009','Une trahison menace de détruire l\amitié entre les garçons et Sheldon, ainsi que les chances du couple que forment Penny et Leonard. Sheldon retourne au Texas en disgrâce quand il apprend que les garçons se sont forcés à le suivre dans son expédition arctique, en ruinant l\espoir de Leonard de retrouvailles romantiques avec Penny mais aussi, et surtout, que les garçons ont menti à Sheldon lors de l\expédition pour qu\il soit content remettant en cause sa découverte scientifique.');
insert into membre(EpisodeID, prenom, nom, role) values(13, 'Johnny','Galecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(13, 'Jim','Parsons', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(13, 'Kunal','Nayyar', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(13, 'Simon','Helberg', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(13, 'Kaley','Cuoco', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(13, 'Laurie','Metcalf', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (14,2,3,'Le Grillon des champs','00:22:00','28/09/2009','Penny et Leonard sont dans une relation compliquée à la suite d\une première nuit difficile. Pendant ce temps, Sheldon et Howard s\affrontent dans un pari pour déterminer les espèces de grillons existantes.');
insert into membre(EpisodeID, prenom, nom, role) values(14, 'Johnny','Galecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(14, 'Jim','Parsons', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(14, 'Simon','Helberg', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(14, 'Kunal','Nayyar', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(14, 'Kaley','Cuoco', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(14, 'Lewis','Black', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (15,2,4,'Le Robot à tout faire !','00:22:00','23/09/2010','Howard, Raj et Leonard s\amusent avec un bras robotique mais Howard lui trouve une tout autre utilisation, alors que Penny découvre que Sheldon a une relation avec une fille, Amy.');
insert into membre(EpisodeID, prenom, nom, role) values(15, 'Johnny','Galecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(15, 'Jim','Parsons', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(15, 'Kunal','Nayyar', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(15, 'Simon','Helberg', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(15, 'Kaley','Cuoco', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(15, 'Melissa','Rauch', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(15, 'Mayim','Bialik', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (16,2,4,'Les Bienfaits de la Cybernétique','00:22:00','23/09/2010','Sheldon s\inquiète de mourir trop tôt pour pouvoir transférer sa conscience dans un robot. Il commence donc à chercher un moyen d\allonger son espérance de vie.');
insert into membre(EpisodeID, prenom, nom, role) values(16, 'Johnny','Galecki', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(16, 'Jim','Parsons', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(16, 'Kunal','Nayyar', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(16, 'Simon','Helberg', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(16, 'Kaley','Cuoco', 'Acteur');



insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (17,3,1,'À toi qui vis 2000 ans plus tard','00:25:35','07/04/2013','Eren Jäger, Mikasa Ackerman et Armin Arlelt sont trois enfants vivants dans une ville fortifiée en paix jusqu\à l\arrivée du Titan Colossal qui va changer le cours de leur vie.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (18,3,1,'Ce jour-là','00:25:35','13/04/2013','Après la chute du Mur Maria, les survivants tentent d\échapper aux titans. Hannes tente de mettre en sécurité Mikasa et Eren malgré les reproches de ce dernier pour la mort de sa mère.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (19,3,1,'Une faible étincelle dans le désespoir','00:25:35','20/04/2013','Eren, Mikasa et Armin rejoignent la 104ème Brigade d\entraînement de l\Armée Humaine où ils débutent leur entraînement.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (20,3,1,'Le soir de la cérémonie de fin d\entraînement','00:25:35','28/04/2013','La 104ème Brigade d\entraînement poursuit ses efforts sous le contrôle de Keith Shadis qui leur fait également passer leur examens.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (21,3,1,'Première bataille','00:25:35','05/05/2013','Après l’apparition du Titan Colossal et d\une brèche le Mur Rose, la 104ème Brigade d\entraînement et la Garnison doivent faire face à une nouvelle attaque des titans, ainsi débute la Bataille de Trost.');



insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (22,5,1,'De la graine de héros','00:22:00','02/12/2013','Rick emménage avec la famille de sa fille et a une mauvaise influence sur son petit fils, Morty.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (23,5,1,'De la graine de héros','00:22:00','09/12/2013','Rick aide Jerry à discipliner son chien, puis s\immisce avec Morty dans les rêves extravagants de M. Goldenfold, son professeur de mathématiques.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (24,5,1,'Anatomy Park','00:22:00','16/12/2013','Dans cet épisode spécial Noël, Rick et Morty essaient de sauver la vie d\un sans-abri. Dans le même temps, les parents de Jerry lui rendent visite, avec une surprise imprévue...');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (25,5,1,'M. Night Shaym-Aliens!','00:22:00','13/01/2014','Rick et Morty cherchent à s\évader d\une simulation de réalité virtuelle mise en place par des extraterrestres qui tentent ainsi d\amener Rick à dévoiler un secret technologique.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (26,5,1,'La boîte à larbins','00:22:00','20/01/2014','Jerry se plaint du fait que Morty passe trop de temps avec Rick au lieu de se rendre utile dans la maison. Rick confie alors à Jerry une « boîte à larbins » qui permet de résoudre tout type de problème. Libéré de ses obligations, Morty est donc libre pour partir à l\aventure mais cette fois, il exige d\être celui qui dirigera le cours des événements.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (27,5,1,'E-Rick-xir d\amour','00:22:00','27/01/2014','Morty convainc Rick de lui confectionner un philtre d\amour afin de gagner le cœur de la fille de ses rêves, Jessica. Le philtre s\avère d\une efficacité qui prend une proportion cataclysmique.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (28,5,2,'Effet Rick-ochet','00:22:00','26/07/2015','Morty et Summer fracturent leur réalité temporelle par inadvertance, tandis que Beth et Jerry tentent de sauver un cerf qu\ils ont heurté en voiture.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (29,5,2,'Prout, l\extra-terrestre','00:22:00','03/08/2015','Rick et Morty déposent Jerry pour la journée dans un établissement spécialisé, puis se rendent à un salon de jeux vidéo, mais Morty décide soudain d\empêcher l\assassinat d\un extra-terrestre télépathe.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (30,5,2,'Assimilation auto-érotique','00:22:00','09/08/2015','Rick renoue les liens avec une ancienne compagne. Beth et Jerry découvrent un laboratoire sous le garage et ce qu\ils y trouvent provoque une dispute.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (31,5,2,'Total Rickall','00:22:00','16/08/2015','La famille fait face à une invasion de parasites extra-terrestres modifiant leur mémoire.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (32,5,2,'On va vous faire schwifter','00:22:00','23/08/2015','Rick et Morty participent à un télé-crochet intergalactique pour sauver la Terre.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (33,5,2,'Les Ricks sont tombés sur la tête','00:22:00','30/08/2015','La soucoupe volante de Rick tombe en panne. Pour la réparer, Rick et Morty se téléportent dans la batterie, en réalité alimenté par une univers miniature créé par Rick. Summer reste dans le vaisseau et tente de rester en sécurité.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (34,5,2,'Mini-Rick, méga hic','00:22:00','13/09/2015','Rick transfère son esprit dans un clone plus jeune pour passer plus temps avec Morty et Summer, mais il semble y prendre un peu trop de plaisir. Beth & Jerry suivent en vain une thérapie de couple.');



insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (35,6,1,'Cartman a une sonde anale','00:20:00','13/08/1997','Cartman raconte à Stan, Kyle et Kenny qu\il a été enlevé par des extraterrestres lors d\un rêve. Les garçons se rendent compte que cela s\est réellement produit quand Ike, le petit frère de Kyle, est kidnappé. Cartman semble s\être fait implanté une sonde anale par les visiteurs.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (36,6,1,'Muscle Plus 4000','00:20:00','20/08/1997','Les habitants de South Park préparent avec enthousiasme la visite de Kathie Lee Gifford, mais M. Garrison met au point un plan afin de l\assassiner et se venger de son succès. Entre-temps, Eric Cartman devient dangereusement obèse après l\achat de Muscle Plus 4000');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (37,6,1,'Volcano','00:20:00','27/08/1997','Stan, Kyle, Cartman et Kenny partent chasser avec Jimbo, l\oncle de Stan, et son ami Ned. Stan se sent frustré par sa difficulté à tirer sur un animal, tandis que Cartman essaie d\effrayer les autres avec l\histoire d\une créature extraordinaire nommée Kukrapok.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (38,6,1,'Une promenade complètement folle avec Al Super Gay','00:20:00','03/09/1997','Le chien de Stan, Sparky s\avère être homosexuel. Sous la pression de ses camarades, Stan essaye de le viriliser, ce qui a pour conséquence de faire fuir Sparky vers le refuge pour animaux gays de Al Super Gay.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (39,6,1,'Un éléphant fait l\amour à un cochon','00:20:00','10/09/1997','Stan, Kyle, Cartman et Kenny parient qu\ils parviendraient à croiser un éléphant avec un cochon avec d\autres enfants, qui eux pensent pouvoir cloner un être humain.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (40,6,1,'Jamais sans mon anus','00:20:00','01/04/1998','Terrance et Philippe sont victimes d\un complot de Scott qui permet à Saddam Hussein d\envahir le Canada.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (41,6,2,'La mère de Cartman est toujours une folle du cul','00:20:00','22/04/1998','Le suspense est à son comble, Mephisto va dévoiler qui est le père de Cartman, lorsque soudain on lui tire dessus.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (42,6,2,'Le Charmeur de poules','00:20:00','20/05/1998','Le bibliobus arrive à South Park. Les enfants n\en ont cure, jusqu\à ce qu\une affaire de viol de poules n\éclate.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (43,6,2,'Le Zizi de Ike','00:20:00','27/05/1998','Kyle apprend que ses parents veulent faire circoncire son frère. Pendant ce temps, M. Mackey découvre la drogue.');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (44,6,2,'Le Fœtus siamo-maxillaire','00:20:00','03/06/1998','L\infirmière Gollum effraie toute la ville. Les gens, la pensant isolée, tentent de l\intégrer à la société.');



insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (45,7,1,'Un enfant non-terrien','00:23:31','23/11/1963','Les professeurs Ian Chesterton et Barbara Wright sont préoccupés par l\une de leurs élèves, Susan Foreman. Elle est précoce, mais semble avoir d\étranges lacunes dans certains domaines fondamentaux. Ils la suivent à son adresse et rencontrent le grand-père de Susan, un mystérieux Docteur...');
insert into membre(EpisodeID, prenom, nom, role) values(45, 'William','Hartnell', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(45, 'Carole','Ann Ford', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(45, 'Jacqueline','Hill', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(45, 'William','Russell', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(45, 'Derek','Newark', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(45, 'Alethea','Charlton', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(45, 'Eileen','Way', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(45, 'Jeremy','Young', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(45, 'Howard','Lang', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (46,7,1,'Les Daleks','00:23:15','21/12/1963','Arrivés sur une planète qu\ils pensent complètement abandonnée, le Docteur et ses compagnons vont devoir trouver du mercure pour réparer le vaisseau. Mais en chemin, ils tombent sur des créatures mutantes étranges, les mystérieux Daleks...');
insert into membre(EpisodeID, prenom, nom, role) values(46, 'William','Hartnell', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(46, 'Carole','Ann Ford', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(46, 'Jacqueline','Hill', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(46, 'Willam','Russell', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(46, 'Alan','Wheatley', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(46, 'John','Lee', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(46, 'Virginia','Wetherell', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(46, 'Philip','Bond', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(46, 'Marcus','Hammond', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (47,7,2,'La planète des géants','00:23:08','31/10/1964','Alors que le TARDIS subit de nombreux problèmes de fonctionnement, le Docteur et ses compagnons sortent explorer le lieu où ils se sont rendus. Après quelques examens des environs, ils se rendent compte qu\ils se trouvent bel et bien dans l\Angleterre du xxe siècle, mais qu\ils ont été miniaturisés dans un jardin où vient de se dérouler un crime.');
insert into membre(EpisodeID, prenom, nom, role) values(47, 'William','Hartnell', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(47, 'Carole','Ann Ford', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(47, 'Jacqueline','Hill', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(47, 'William','Russell', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(47, 'Alan','Tilvern', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(47, 'Frank','Crawshaw', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(47, 'Reginald','Barratt', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(47, 'Rosemary','Johnson', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (48,7,2,'L\invasion de la terre par les daleks','00:23:37','21/11/1964','L\équipage du TARDIS se retrouve dans un Londres du xxiie siècle calme et en ruines. Ils ne s\aperçoivent que bien trop tard que dans ce futur, la Terre a été envahie par les Daleks.');
insert into membre(EpisodeID, prenom, nom, role) values(48, 'William','Hartnell', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(48, 'Carole','Ann Ford', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(48, 'Jacqueline','Hill', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(48, 'Bernard','Kay', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(48, 'Peter','Fraser', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(48, 'Alan','Judd', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(48, 'Ann','Davies', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(48, 'Michael','Goldie', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(48, 'Michael','Davis', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(48, 'Richard','McNeff', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(48, 'Graham','Rigby', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (49,7,3,'Galaxie 4','00:23:55','11/09/1965','Le Docteur et ses compagnons atterrissent sur une planète inconnue sur le point d\exploser dans laquelle un conflit entre les guerrières Drahvins et d\étranges créatures nommées les Rills semble avoir atteint son apogée.');
insert into membre(EpisodeID, prenom, nom, role) values(49, 'William','Hartnell', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(49, 'Maureen','O\Brien', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(49, 'Peter','Purves', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(49, 'Stephanie','Bidmead', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (50,7,3,'Mission dans l\inconnu','00:24:27','05/10/1965','Sur la planète Kembel, trois astronautes terriens perdus cherchent à réparer leur vaisseau. L\un d\entre eux sera témoin d\une alliance secrète entre les Daleks et différentes races de l\univers dont le plan est de conquérir et détruire la galaxie, en commençant par la Terre.');
insert into membre(EpisodeID, prenom, nom, role) values(50, 'Barry','Jackson', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(50, 'Edward','de Souza', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(50, 'Jeremy','Young', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(50, 'Robert','Cartland', 'Acteur');



insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (51,8,1,'Rose','00:45:00','26/03/2005','Rose une jeune vendeuse du xxie siècle croise la route du Docteur qui la sauve des Autons. Elle mène alors l\enquête pour savoir qui est ce mystérieux personnage');
insert into membre(EpisodeID, prenom, nom, role) values(51, 'Christopher','Eccleston', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(51, 'Noel','Clarke', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(51, 'Billie','Piper', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(51, 'Camille','Coduri', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (52,8,1,'La Fin du monde','00:45:00','2/04/2005','Le Docteur donne à Rose le choix de voir le futur ou le passé. Elle choisit alors l\avenir et après avoir fait de brefs arrêts, ils s\arrêtent 5 milliards d\années plus tard. Le Docteur lui annonce qu\elle va assister à la cérémonie de mort de la Terre. Cependant cette cérémonie où sont invités Jabe (une femme arbre), le Moxx de Balhoon, les adhérents de la transmission du Meme, Face de Boe… ainsi que Lady Cassandra O\Brien, la dernière survivante de la race humaine, ne va pas se dérouler comme prévu.');
insert into membre(EpisodeID, prenom, nom, role) values(52, 'Christopher','Eccleston', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(52, 'Billie','Piper', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(52, 'Zoë','Wanamaker', 'Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (53,8,3,'La Loi des Judoons','00:45:00','31/03/2007','Alors que le Docteur est en visite dans un hôpital, celui-ci est téléporté sur la Lune. Il y fait la rencontre d\une jeune médecin, Martha Jones.');

insert into membre(EpisodeID, prenom, nom, role) values(53,'David','Tennant','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(53,'Freema','Agyeman','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(53,'Adjoa','Andoh','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(53,'Gugu','Mbatha-Raw','Acteur');



insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (54,9,1,'Le Gundam se dresse','00:24:00','07/04/1979',NULL);

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (55,9,1,'Ordre de destruction','00:24:00','14/04/1979',NULL);

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (56,9,1,'Abattez le ravitailleur !','00:24:00','21/04/1979',NULL);



insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (57,10,1,'Le Gundam noir','00:24:00','02/03/1985',NULL);

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (58,10,1,'Départ','00:24:00','09/03/1985',NULL);

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (59,10,1,'Dans la capsule','00:24:00','16/03/1985',NULL);



insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (60,11,1,'Prélude à ZZ','00:24:00','01/03/1986',NULL);

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (61,11,1,'Le Jeune Homme de Shangri-La','00:24:00','08/03/1986',NULL);

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (62,11,1,'Le Roi de l’Endra','00:24:00','15/03/1986',NULL);



insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (63,12,1,'Effectuer la décision','01:21:52','02/05/2007','Le professeur recrute une jeune braqueuse et sept autres criminels en vue d’un cambriolage grandiose ciblant la Maison royale de la Monnaie d’Espagne.');
insert into membre(EpisodeID, prenom, nom, role) values(63,'Úrsula','Corberó','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(63,'Itziar','Ituño','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(63,'Álvaro','Morte','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(63,'Paco','Tous','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(63,'Pedro','Alonso','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(63,'Alba','Flores','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(63,'Miguel','Herrán','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(63,'Jaime','Lorente','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(63,'Esther','Acebo','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(63,'Enrique','Arce','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(63,'María','Pedraza','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(63,'Darko','Peric','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(63,'Kiti','Mánver','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(63,'Roberto','García','Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (64,12,1,'Des imprudences mortelles','01:30:32','09/05/2007','Raquel, qui gère les négociations pour la libération des otages, établit un contact avec le Professeur. L’un des otages est un élément clé du plan des cambrioleurs.');
insert into membre(EpisodeID, prenom, nom, role) values(64,'Úrsula','Corberó','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(64,'Itziar','Ituño','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(64,'Álvaro','Morte','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(64,'Paco','Tous','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(64,'Pedro','Alonso','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(64,'Alba','Flores','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(64,'Miguel','Herrán','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(64,'Jaime','Lorente','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(64,'Esther','Acebo','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(64,'Enrique','Arce','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(64,'María','Pedraza','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(64,'Darko','Peric','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(64,'Kiti','Mánver','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(64,'Roberto','García','Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (65,12,1,'Erreur après avoir fait feu','01:23:43','16/05/2007','La police identifie un des cambrioleurs. Raquel se méfie de l’homme qu’elle a rencontré dans un bar, qui n’est nul autre que le Professeur.');
insert into membre(EpisodeID, prenom, nom, role) values(65,'Úrsula','Corberó','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(65,'Itziar','Ituño','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(65,'Álvaro','Morte','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(65,'Paco','Tous','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(65,'Pedro','Alonso','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(65,'Alba','Flores','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(65,'Miguel','Herrán','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(65,'Jaime','Lorente','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(65,'Esther','Acebo','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(65,'Enrique','Arce','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(65,'María','Pedraza','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(65,'Darko','Peric','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(65,'Kiti','Mánver','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(65,'Roberto','García','Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (66,12,1,'Un cheval de Troie','01:20:50','23/05/2007','Raquel traverse une crise personnelle avec son ex. Les otages prennent peur en entendant des coups de feu. Les cambrioleurs n’arrivent pas à se mettre d’accord.');
insert into membre(EpisodeID, prenom, nom, role) values(66,'Úrsula','Corberó', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(66,'Itziar','Ituño', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(66,'Álvaro','Morte', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(66,'Paco','Tous', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(66,'Pedro','Alonso', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(66,'Alba','Flores', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(66,'Miguel','Herrán', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(66,'Jaime','Lorente', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(66,'Esther','Acebo', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(66,'Enrique','Arce', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(66,'María','Pedraza', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(66,'Darko','Peric', 'Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(66,'Kiti','Mánver','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(66,'Roberto','García', 'Acteur');


insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (67,13,1,'Cap à l\ouest','01:24:00','03/03/2013','Ragnar Lothbrok et son frère Rollo participent à une bataille contre les peuples Baltes. Après le massacre, Ragnar a des visions du dieu Odin et ses Valkyries. De retour dans son village, Ragnar se rend, en compagnie de son fils Bjorn, à Kattegat pour assister au Thing et afin que ce dernier subisse son rite de passage vers l\âge adulte. Restée à la ferme familiale, la femme de Ragnar, Lagertha, met en fuite deux vagabonds qui tentent de la violer. A Kattegat, Ragnar convainc Rollo que les raids vers l\ouest sont plus prometteurs en termes de butin que les attaques traditionnelles contre les populations Baltes. Il affirme qu\il est capable de trouver son cap en pleine mer grâce à un instrument qu\il s\est procuré auprès d\un voyageur, un compas de navigation. Il est cependant réprimandé et menacé par son chef de clan, le Jarl Haraldson, qui ne croit pas à la présence de terres à l\ouest et souhaite poursuivre ses raids vers l\est. Bjorn et Ragnar rendent visite à Floki, un charpentier de marine visionnaire, qui, grâce au soutien financier de son ami Ragnar, a secrètement entrepris la construction d\un nouveau type de navire, plus adapté à la navigation en haute mer : un drakkar. Les premiers tests en mer sont très prometteurs. Tandis que Ragnar recrute des hommes, son frère Rollo fait des avances importunes à Lagertha. Le navire étant fin prêt, Ragnar met le cap à l\ouest, vers l\inconnu, tandis que le Jarl Haraldson, mis au courant par un traître, commence à tirer vengeance de ceux qui ont aidé Ragnar à monter son expédition.');
insert into membre(EpisodeID, prenom, nom, role) values(67,'Travis','Fimmel','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(67,'Clive','Standen','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(67,'Katheryn','Winnick','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(67,'Gabriel','Byrne','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(67,'Jessalyn','Gilsig','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(67,'Gustaf','Skarsgård','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(67,'George','Blagden','Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (68,13,1,'L\Expédition','01:24:00','10/03/2013','A bord de leur drakkar, Ragnar, Rollo, Floki et les guerriers vikings qui les accompagnent affrontent les eaux déchainées de la mer de Nord, cap à l\ouest. Ragnar est obligé de tuer l\un de ses hommes, qui remettait en cause son autorité en doutant de l\existence d\une terre au bout de l\horizon. Mais des mouettes signalent finalement la proximité d\une côte : c\est le nord-est de l\Angleterre, qui appartient au royaume de Northumbrie. Les vikings abordent à proximité du monastère de Lindisfarne, qui abrite une petite communauté de moines chrétiens. Ils pillent les lieux et massacrent les moines, n\en conservant qu\une poignée en vie, qui sont emmenés comme esclaves. Rollo et Ragnar s\affrontent à propos d\un des moines, le frère Athelstan, qui parle la langue des vikings et que Ragnar souhaite conserver en vie. Chargé d\esclaves et de butin, le drakkar prend le chemin du retour vers Kattegat.');
insert into membre(EpisodeID, prenom, nom, role) values(68,'Travis','Fimmel','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(68,'Clive','Standen','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(68,'Katheryn','Winnick','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(68,'Gabriel','Byrne','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(68,'Jessalyn','Gilsig','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(68,'Gustaf','Skarsgård','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(68,'George','Blagden','Acteur');


insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (69,14,1,'Abysses','00:10:00',NULL,NULL);

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (70,14,1,'Une souris et un homme','00:10:00',NULL,NULL);


insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (71,15,1,'Je ne suis pas un monstre','00:21:00','21/11/1999','À l\école, le professeur de la classe de surdoués découvre que Malcolm est un génie. À son grand désespoir, il intègre la classe. Il devient alors ami avec Stevie Kenerban, une « tête d\ampoule » handicapée et asthmatique. Alors que Malcolm est désespéré par sa nouvelle classe, il découvre finalement qu\elle n\est pas aussi terrible qu\il le pensait.');
insert into membre(EpisodeID, prenom, nom, role) values(71,'Frankie','Muniz','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(71,'Jane','Kaczmarek','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(71,'Bryan','Cranston','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(71,'Christopher','Kennedy Masterson','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(71,'Justin','Berfield','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(71,'Erik','Per Sullivan','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(71,'Catherine','Lloyd Burns','Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (72,15,1,'Alerte rouge','00:21:00','28/11/1999','C\est l\anniversaire du mariage de Hal et Loïs. Hal prévoit une soirée romantique avec sa femme au restaurant, mais Lois découvre que sa robe de soirée a brûlé et est cachée dans la cuvette des toilettes. Elle passe alors sa soirée à punir Reese, Malcolm et Dewey pour savoir qui est le responsable en les torturant. Malcolm, Reese et Dewey font appel à Francis mais Loïs le découvre, pendant que Hal l\attend au restaurant, enchainant les verres. Elle redouble d\ingéniosité pour punir les garçons, ceux-ci ne voulant pas avouer qui est le coupable. Ils finissent par appeler Francis pour qu\ils les aident à faire craquer leur mère. On apprend finalement que c\est Hal qui est responsable de l\incendie de la robe de Loïs.');
insert into membre(EpisodeID, prenom, nom, role) values(72,'Frankie','Muniz','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(72,'Jane','Kaczmarek','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(72,'Bryan','Cranston','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(72,'Christopher','Kennedy Masterson','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(72,'Justin','Berfield','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(72,'Erik','Per Sullivan','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(72,'Catherine','Lloyd Burns','Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (73,16,1,'Chute libre','00:43:00','20/01/2018','Walter White, 50 ans, est Professeur de chimie dans un lycée public. Il travaille parallèlement dans une station de lavage de voitures afin de boucler les fins de mois. Il vit avec sa femme Skyler qui est enceinte de leur deuxième enfant et son fils Walter Jr, un adolescent handicapé.
Son beau-frère, Hank Schrader est un agent fédéral de la DEA. Il l\invite à participer à l\interpellation d\un petit producteur de méth. A cette occasion, il revoit un ancien élève à lui, Jesse Pinkman, qui est vraisemblablement devenu dealer de drogues.Il découvre alors qu\il est atteint d\un cancer du poumon en stade terminal. Le traitement est particulièrement onéreux et sa famille a déjà du mal financièrement. Inquiet pour leur futur et inspiré par les révélations de son beau-frère sur le business de la drogue, il décide d\utiliser ses connaissances en chimie pour fabriquer de la méth. Il contacte alors son ancien élève, Jesse, pour lui proposer de faire affaire avec lui.');
insert into membre(EpisodeID, prenom, nom, role) values(73,'Bryan','Cranston','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(73,'Aaron','Paul','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(73,'Anna','Gunn','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(73,'Dean','Norris','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(73,'Betsy','Brandt','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(73,'Roy','Frank Mitte','Acteur');

insert into episode(EpisodeID,SerieID,saison,Titre_Episode,duree,DateDiffusion,resume) values (74,16,1,'Le Choix','00:55:00','27/01/2018','Walt et Jesse décident de cuisiner dans un camping-car au fond du désert. Jesse prend contact avec un revendeur de drogue, Krazy-8, afin d\écouler leur petit stock. Imprudent, il conduit Krazy-8 et son cousin Emilio jusqu\au camping-car où se trouve Walt. L\entrevue tourne mal et Walt parvient à créer un gaz qui tue les deux voyous. Mais ils doivent se débarrasser des corps. Leur tâche se complique quand ils découvrent avec stupéfaction que l\un d\entre eux a survécu. Les deux acolytes ne sont pas d\accord quant à la marche à suivre.
De son côté, Skyler s\interroge sur le comportement étrange de son mari. Intriguée par un appel, elle mène l\enquête pour découvrir ce qu\il fabrique et découvre qu\il fréquente Jesse Pinkman et son site internet.');
insert into membre(EpisodeID, prenom, nom, role) values(74,'Bryan','Cranston','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(74,'Aaron','Paul','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(74,'Anna','Gunn','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(74,'Dean','Norris','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(74,'Betsy','Brandt','Acteur');
insert into membre(EpisodeID, prenom, nom, role) values(74,'Roy','Frank Mitte','Acteur');

insert into Users(pseudo,sexe,DateSignUp,Age) values('Azrod95','M','19/12/1968',29);
insert into Users(pseudo,sexe,DateSignUp,Age) values('Kikoo','M','29/09/2015',15);
insert into Users(pseudo,sexe,DateSignUp,Age) values('Marion77','F','29/09/2015',64);
insert into Users(pseudo,sexe,DateSignUp,Age) values('Lola123','F','29/09/2015',40);
insert into Users(pseudo,sexe,DateSignUp,Age) values('Julia12','F','02/06/2009',21);
insert into Users(pseudo,sexe,DateSignUp,Age) values('Eustache29','M','14/12/2010',21);




insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(1,NULL,'Julia12','pas mal','01/02/2019',5);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(1,NULL,'Kikoo','WOAW !!!','15/02/2017',7);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(2,NULL,'Lola123','j\aime bien','21/06/2019',10);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(2,NULL,'Marion77',NULL,'04/11/2019',8);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(2,NULL,'Kikoo','TRO BI1','13/02/2018',10);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(2,NULL,'Azrod95',NULL,'01/01/2020',6);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(3,NULL,'Julia12','pas mal','28/09/2019',5);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note)
values(3,NULL,'Lola123',NULL,'19/04/2019',9);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(5,NULL,'Lola123','pas mal','18/02/2016',5);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(5,NULL,'Julia12','pas mal','17/01/2019',5);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(6,NULL,'Julia12','pas mal','14/08/2018',5);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(7,NULL,'Julia12','j\ai rien compris','06/12/2019',2);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(8,NULL,'Julia12','pas mal','08/01/2020',5);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(9,NULL,'Julia12','pas mal','01/03/2014',5);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(10,NULL,'Julia12','pas mal','01/01/2014',5);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(11,NULL,'Julia12','pas mal','21/04/2018',5);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(12,NULL,'Julia12','pas mal','03/07/2019',5);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(13,NULL,'Julia12','pas mal','11/05/2019',5);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(14,NULL,'Julia12','pas mal','07/12/2017',5);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(15,NULL,'Julia12','pas mal','05/11/2017',5);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(16,NULL,'Eustache29','excellent my froueennd','26/01/2020',10);



insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(16,74,'Marion77','Enfin de l\action !','18/05/2018',9);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(16,73,'Marion77',NULL,'18/05/2018',8);
insert into Avis(SerieID,EpisodeID,Pseudo,Commentaire,DateComment,Note) values(12,66,'Marion77',NULL,'21/04/2018',4);




insert into ForumMessage(ForumMessageID,Pseudo,Message,DateMessage,MessageRepondu) values(1,'Azrod95','c\est quoi la serie avec bébé Yoda ?','10/12/1998',NULL);
insert into ForumMessage(ForumMessageID,Pseudo,Message,DateMessage,MessageRepondu) values(2,'Kikoo','C THE MANDALORIAN','12/07/1999',1);
insert into ForumMessage(ForumMessageID,Pseudo,Message,DateMessage,MessageRepondu) values(3,'Azrod95','Malcolm est bien comme serie ?','14/08/1999',NULL);
insert into ForumMessage(ForumMessageID,Pseudo,Message,DateMessage,MessageRepondu) values(4,'Julia12','ouais c\est pas mal','16/08/2000',3);
insert into ForumMessage(ForumMessageID,Pseudo,Message,DateMessage,MessageRepondu) values(5,'Azrod95','Ok merci Kikoo','13/09/1999',2);
insert into ForumMessage(ForumMessageID,Pseudo,Message,DateMessage,MessageRepondu) values(6,'Lola123','c\est moi ou tu sais que dire c\est pas mal Julia12 ? XD','15/11/2017',4);
insert into ForumMessage(ForumMessageID,Pseudo,Message,DateMessage,MessageRepondu) values(7,'Marion77','c\est plus que pas mal Malcolm c\est super drole !','17/12/2017',4);
insert into ForumMessage(ForumMessageID,Pseudo,Message,DateMessage,MessageRepondu) values(8,'Kikoo','PTDRRRRRRRRR C TRO VRÉ MDRRRRRRRRRRRRRR LOOOOOOOL XD','03/01/2018',6);
insert into ForumMessage(ForumMessageID,Pseudo,Message,DateMessage,MessageRepondu) values(9,'Eustache29','Yes. no. maybe. i don\t know. can you reapeat the question?','14/08/1999',3);

insert into Forumsujet(ForumMessageID,sujet,SerieID) values(1,'Recherche de series',NULL);
insert into Forumsujet(ForumMessageID,sujet,SerieID) values(3,'Avis pour Malcolm',16);